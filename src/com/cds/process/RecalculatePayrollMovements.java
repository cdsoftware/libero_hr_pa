package com.cds.process;

import java.util.logging.Level;

import org.compiere.model.MPeriod;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.eevolution.model.MHRPeriod;
import org.eevolution.model.MHRProcess;

/** Proceso que recibe como parametro un proceso planilla y opcionalmente un empleado.
 * 	Recalcula los movimentos de planilla basado en los valores modificados en los movimientos que vienen de atributos base
 * @author angel
 *
 */
public class RecalculatePayrollMovements extends SvrProcess{
	
	int p_HR_process_ID = 0;
	int p_C_BPartner_ID = 0;	
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("HR_process_ID"))
				p_HR_process_ID = para[i].getParameterAsInt();
			if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		
		MHRProcess hrprocess = new MHRProcess(getCtx(), p_HR_process_ID, get_TrxName());
		
		// Std Period open?
		MHRPeriod period = MHRPeriod.get(getCtx(), hrprocess.getHR_Period_ID());
		MPeriod.testPeriodOpen(getCtx(),
				hrprocess.getHR_Period_ID() > 0 ? period.getDateAcct() : hrprocess.getDateAcct(),
						hrprocess.getC_DocTypeTarget_ID(), hrprocess.getAD_Org_ID());
		
		
		return null;
	}

}
